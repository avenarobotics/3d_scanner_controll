from dynamixel_sdk.packet_handler import PacketHandler
from dynamixel_sdk.port_handler import PortHandler
from dynamixel_sdk.robotis_def import COMM_SUCCESS
import photo_capture
import time
from datetime import datetime
import tqdm
import os
from pynput.keyboard import Key, Listener

DXL1_MINIMUM_POSITION_VALUE = 0  # Dynamixel will rotate between this value
DXL1_MAXIMUM_POSITION_VALUE = 4095
MOVEMENT_END_ROT = 0
dxl1_goal_position = [DXL1_MINIMUM_POSITION_VALUE, DXL1_MAXIMUM_POSITION_VALUE]
STOP_TORQUE = Key.esc
STOP_TORQUE_BOOL = False

def on_press(key):
    pass

def on_release(key, dynamixel):
    global STOP_TORQUE_BOOL
    if key == Key.esc:
        print('{0} pressed, stopping torque'.format(
            key))
        STOP_TORQUE_BOOL = True

        return False

class ER_Dynamixel():
    def __init__(self, dxl_id, device_port, baudrate):

        self.ADDR_XL_TORQUE_ENABLE = 64
        self.ADDR_XL_GOAL_POSITION = 116
        self.ADDR_PRO_PRESENT_POSITION = 132
        self.OPERATING_MODE = 11

        self.TORQUE_ENABLE = 1
        self.VELOCITY_ENABLE = 1
        self.MOVEMENT_START = 0
        self.MOVEMENT_END = 4095 * 4

        # Protocol version
        PROTOCOL_VERSION = 2

        # Default setting
        self.DXL_ID = dxl_id  # Dynamixel ID

        # Initialize PortHandler instance
        self.portHandler = PortHandler(device_port)

        # Initialize PacketHandler instance
        self.packetHandler = PacketHandler(PROTOCOL_VERSION)

        # Open port
        if self.portHandler.openPort():
            print("Succeeded to open the port")
        else:
            print("Failed to open the port")
            quit()

        # Set port baudrate
        if self.portHandler.setBaudRate(baudrate):
            print("Succeeded to change the baudrate")
        else:
            print("Failed to change the baudrate")
            quit()

    def __del__(self):
        # Close port
        self.portHandler.closePort()

    def enable_torque(self, on_off):
        # Enable Dynamixel Torque
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(
            self.portHandler, self.DXL_ID, self.ADDR_XL_TORQUE_ENABLE, on_off)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        else:
            print("Torque mode", on_off)

    def change_operation_mode(self, mode):
        # Enable Dynamixel Torque
        dxl_comm_result, dxl_error = self.packetHandler.write1ByteTxRx(
            self.portHandler, self.DXL_ID, 11, mode)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        else:
            print("Velocity mode ", mode)

    def set_velocity(self, target_velocity):
        dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(
            self.portHandler, self.DXL_ID, 104, target_velocity)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))

    def set_position(self, position):
        dxl_comm_result, dxl_error = self.packetHandler.write4ByteTxRx(
            self.portHandler, self.DXL_ID, self.ADDR_XL_GOAL_POSITION, position)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))

    def get_position(self):
        dxl2_present_position, dxl_comm_result, dxl_error = self.packetHandler.read4ByteTxRx(self.portHandler,
                                                                                             self.DXL_ID,
                                                                                             self.ADDR_PRO_PRESENT_POSITION)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))
        return dxl2_present_position

    def get_opertaion_mode(self):
        dxl2_operation_mode, dxl_comm_result, dxl_error = self.packetHandler.read1ByteTxRx(self.portHandler,
                                                                                           self.DXL_ID,
                                                                                           11)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))

        return dxl2_operation_mode

    def get_torque_enabled(self):
        dxl2_torque_mode, dxl_comm_result, dxl_error = self.packetHandler.read1ByteTxRx(self.portHandler,
                                                                                           self.DXL_ID,
                                                                                           64)
        if dxl_comm_result != COMM_SUCCESS:
            print("%s" % self.packetHandler.getTxRxResult(dxl_comm_result))
        elif dxl_error != 0:
            print("%s" % self.packetHandler.getRxPacketError(dxl_error))

        return dxl2_torque_mode

def make_circle(dynamixel, camera_name1, camera_name2, two_cameras, speed, forward=True):
    start_position = dynamixel.get_position()
    print("Current dynamixel position: ", start_position)

    # disable torque if enabled
    if dynamixel.get_torque_enabled() == 1:
        dynamixel.enable_torque(0)

    # change mode to velocity
    if dynamixel.get_opertaion_mode() != 1:
        dynamixel.change_operation_mode(1)

    # enable torque
    if dynamixel.get_torque_enabled() == 0:
        dynamixel.enable_torque(1)

    begin_time = time.time()
    stop_pos_list = list()
    rev_stop_pos_list = list()

    for i in range(23):
        stop_pos_list.append(start_position + i*819)
        rev_stop_pos_list.append(start_position - i*819)

    # start photo rotation
    if forward:
        for pos in stop_pos_list:
            dynamixel.set_velocity(speed)
            while dynamixel.get_position() < pos:
                pass
            dynamixel.set_velocity(0)
            time.sleep(4)
            print("Dynamixel position: ", dynamixel.get_position())
            if not two_cameras:
                filename = os.path.join('dupa' + str(datetime.now().strftime("%m:%d:%H:%M:%S.%f")) + '.jpg')
                photo_capture.capture_photo('', filename)
            else:
                photo_capture.capture_photo_two_cameras(camera_name1, camera_name2, two_cameras)


    else:
        for pos in rev_stop_pos_list:
            dynamixel.set_velocity(-speed)
            while dynamixel.get_position() > pos:
                pass
            dynamixel.set_velocity(0)
            time.sleep(4)
            if not two_cameras:
                filename = os.path.join('dupa' + str(datetime.now().strftime("%m:%d:%H:%M:%S.%f")) + '.jpg')
                photo_capture.capture_photo('', filename)
            else:
                photo_capture.capture_photo_two_cameras(camera_name1, camera_name2, two_cameras)
            print("Dynamixel final position: ", dynamixel.get_position())

    print("Dynamixel position: ", dynamixel.get_position())

def main(camera_name1, camera_name2, two_cameras):
    dynamixel = ER_Dynamixel(2, "/dev/ttyUSB0", 115200)
    make_circle(dynamixel, camera_name1=camera_name1, camera_name2=camera_name2, two_cameras=two_cameras, speed=15, forward=True)

    # filename = os.path.join('dupa' + str(datetime.now().strftime("%m:%d:%H:%M:%S.%f")) + '.jpg')
    #
    # if not two_cameras:
    #     photo_capture.capture_photo('', filename)
    # else:
    #     photo_capture.capture_photo_two_cameras(camera_name1, camera_name2, two_cameras)


    # while dynamixel.get_position() < 4095:
    #     print(dynamixel.get_position())
    # dynamixel.set_velocity(0)

    # while position < 4095:
    #     position = dynamixel.get_position()
    # dynamixel.set_position(0)
    # dynamixel.set_velocity(0)
    # dynamixel.set_position(dynamixel.MOVEMENT_START + dynamixel.get_position())
    #
    # number_of_step_to_make = 40
    # full_circle_dynamixel_value = 4095
    # gear_ratio = 4
    #
    # step_with_gear_ratio = int(full_circle_dynamixel_value * gear_ratio / number_of_step_to_make)
    #
    # step = -step_with_gear_ratio if reverse else step_with_gear_ratio
    # dynamixel_start_range = dynamixel.MOVEMENT_START + dynamixel.get_position()
    # dynamixel_end_range = dynamixel.MOVEMENT_START - dynamixel.MOVEMENT_END - dynamixel.get_position() if reverse else dynamixel.MOVEMENT_END + dynamixel.get_position()
    # # dynamixel.set_position(40)
    # # print(dynamixel.get_position())
    # i=0
    # for current_position in tqdm.tqdm(range(dynamixel_start_range, dynamixel_end_range, step)):
    #     dynamixel.set_position(current_position)
    #     print(dynamixel.get_position())
    #     time.sleep(2)
    #     filename = os.path.join('dupa' + str(datetime.now().strftime("%m:%d:%H:%M:%S.%f")) + '.jpg')
    #     MOVEMENT_END_ROT = dynamixel.MOVEMENT_END
    # if not two_cameras:
    #     photo_capture.capture_photo('', filename)
    # else:
    #     photo_capture.capture_photo_two_cameras(camera_name1, camera_name2, two_cameras)

    # while True:
    #     dynamixel.start_dxl()
    #     time.sleep(0.1)
    #     dynamixel.stop_dxl()
    #     time.sleep(0.5)
    #     time.sleep(2.0)


if __name__ == "__main__":
    # If you use two cameras set two_cameras to True. Otherwise set two_cameras to False
    two_cameras = True

    camera_name1 = ' --port ' + 'usb:003,023'
    if two_cameras:
        camera_name2 = ' --port ' + 'usb:003,022'
    else:
        camera_name2 = None

    # camera_name1 = None
    # camera_name2 = None

    main(camera_name1, camera_name2, two_cameras)
